import { ModelType } from '../enums/model-type.enum';

export interface LegalPerson {
    id: number;
    firstname: string;
    lastName: string;
    modelType: ModelType;
    setWorkMod_ids: Array<number>;
}