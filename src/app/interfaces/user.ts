import { UserRole } from '../enums/user-role.enum';

export interface User {
    id: number;
    firstname: string;
    lastName: string;
    login: string;
    password: string;
    userGallery_id: number;
    userRole: string;
}
