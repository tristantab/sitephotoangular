export interface Photo {
    id: number;
    title: string;
    locationTaken: string;
    dateTaken: Date;
    description: string;
    // setGalleries_ids: Set<number>; // as imagerRef for galleries
    // setCommentaries_ids: Set<number>;
    // setUsersAccess_ids: Set<number>;
    setUsersAccess_ids: Array<number>;
    // setModels_ids: Set<number>;
    urlLocation: string;
    refInstagram: number;
    forSale: boolean;
    nbSold: number;
}
