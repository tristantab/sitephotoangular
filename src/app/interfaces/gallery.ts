
export interface Gallery {
    id: number;
    title: string;
    description: string;
    imageRef_id: number;
    setWorkGal_ids: Array<number>;
    user_id: number;
}
