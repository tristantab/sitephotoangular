
export interface Commentary {
    id: number;
    comment: string;
    user_id: number;
    work_id: number;
}
