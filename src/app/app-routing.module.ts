import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { PhotoComponent } from './components/photo/photo.component';
import { UsersComponent } from './components/users/users.component';
import { LegalPersonsComponent } from './components/legal-persons/legal-persons.component';
import { CommentariesComponent } from './components/commentaries/commentaries.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full' },
  {path: 'gallery/:id', component: GalleryComponent},
  {path: 'photo/:id', component: PhotoComponent},
  {path: 'users', component: UsersComponent},
  {path: 'models', component: LegalPersonsComponent},
  {path: 'commentaries', component: CommentariesComponent},
  {path: 'home', component: HomeComponent},
  {path: '**', component: HomeComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
