import { Component, OnInit, OnChanges } from '@angular/core';
import { Gallery } from 'src/app/interfaces/gallery';
import { Photo } from 'src/app/interfaces/photo';
import { BdService } from 'src/app/services/bd.service';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { of } from 'rxjs';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit {

  _galleryChosen: Gallery;
  _setWorkGalChosen: Array<Photo> = new Array();
  _photo: Photo;
  // baseUrl = 'http://localhost:4200';

  get galleryChosen() {
    return this._galleryChosen;
  }
  get setWorkGalChosen() {
    return this._setWorkGalChosen;
  }
  get photo() {
    return this._photo;
  }


  constructor(private bd: BdService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    /* get gallery with id from url parameter */
    this.loadingGalChosen();

    // check router changes and update gallery
    // https://stackoverflow.com/questions/46969864/on-query-parameters-change-route-is-not-updating
    // https://www.amadousall.com/angular-routing-how-to-display-a-loading-indicator-when-navigating-between-routes/
    this.router.events.subscribe((event) => {
      switch (true) {
        case event instanceof NavigationEnd : {
          this.loadingGalChosen();
          // console.log(event);
          break;
        }
        default: {
          break;
        }
      }
    });

  }

  /* get galleryChosen from db */
  loadingGalChosen() {

    const id = this.route.snapshot.params['id'];
    this.bd.getGalleryById(+id).subscribe((response) => {
      // console.log(response);
      this._galleryChosen = <Gallery>response;
      console.log(this._galleryChosen.title);
      // console.log(this._galleryChosen.setWorkGal_ids);

      /* get images from setWorkGal_ids */
      this._setWorkGalChosen = [];
      let i: number;
      for (i of this.galleryChosen.setWorkGal_ids) {
          this.bd.getPhotoById(i).subscribe((response2) => {
            this._setWorkGalChosen.push(<Photo>response2);
            this._photo = <Photo>response2;
            // console.log('the image : ' + this._photo.title);
            // console.log('link to image display page : ' + this.baseUrl + '/photo/' + this._photo.id);
          });
        }
        // console.log('setWork saved :' + this._setWorkGalChosen);
    });

  }

}
