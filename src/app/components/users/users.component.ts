import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { BdService } from 'src/app/services/bd.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  connectedUser: User;
  _users: Array<User> = new Array();

  get users() {
    return this._users;
  }

  constructor(private bd: BdService) { }

  ngOnInit() {

    this.bd.getUsers().subscribe((response) => {
      this._users = response;
      // test profil choisi
      this.connectedUser = this._users[0];
    });

  }

}
