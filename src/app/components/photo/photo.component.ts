import { Component, OnInit, Input } from '@angular/core';
import { Photo } from 'src/app/interfaces/photo';
import { BdService } from 'src/app/services/bd.service';
import { ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';

@Component({
  selector: 'app-photo',
  templateUrl: './photo.component.html',
  styleUrls: ['./photo.component.css']
})
export class PhotoComponent implements OnInit {

  _photoChosen: Photo;
  get photoChosen() {
    return this._photoChosen;
  }


  constructor(private bd: BdService, private route: ActivatedRoute, private _location: Location) { }

  ngOnInit() {
    this.blockRightClick();
    let id = this.route.snapshot.params['id'];
    // console.log(id);
    this.bd.getPhotoById(+id).subscribe((response) => {
      // console.log(response);
      this._photoChosen = <Photo> response;
      console.log(this._photoChosen.title);
    });

  }

  goBackUrl() {
    this._location.back();
  }

  blockRightClick() {
    if (document.addEventListener) { // IE >= 9; other browsers
      document.addEventListener('contextmenu', function(e) {
          alert("You've tried to open context menu"); //here you draw your own menu
          e.preventDefault();
      }, false);
    } 
    // else { // IE < 9
    //     document.attachEvent('oncontextmenu', function() {
    //         alert("You've tried to open context menu");
    //         window.event.returnValue = false;
    //     });
    // }
  }


}
