import { Component, OnInit } from '@angular/core';
import { Gallery } from 'src/app/interfaces/gallery';
import { BdService } from 'src/app/services/bd.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  _galleries: Array<Gallery> = new Array();
  _setRefPhotoPerGal: Array<String> = new Array();
  baseUrl = 'http://localhost:4200';

  get galleries() {
    return this._galleries;
  }
  get setRefPhotoPerGal() {
    return this._setRefPhotoPerGal;
  }


  constructor(private bd: BdService, private route: ActivatedRoute) { }

  ngOnInit() {

    /* get galleries */
    this.bd.getGalleries().subscribe((response) => {
      this._galleries = <Array<Gallery>> response;

      /* get ref image per gallery */
      let gal: Gallery;
      for (gal of this._galleries) {
          this.bd.getPhotoById(gal.imageRef_id).subscribe((response2) => {
            this._setRefPhotoPerGal.push(response2.urlLocation);
            // console.log('the image : ' + <Photo>response2.title);
            // console.log('link to image display page : ' + this.baseUrl + '/photo/' + this._photo.id);
          });
        }
        // console.log('setWork saved :' + this._setRefPhotoPerGal);
    });

  }

}
