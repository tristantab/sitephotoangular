import { Component, OnInit } from '@angular/core';
import { LegalPerson } from 'src/app/interfaces/legal-person';
import { BdService } from 'src/app/services/bd.service';
import { Photo } from 'src/app/interfaces/photo';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { modelGroupProvider } from '@angular/forms/src/directives/ng_model_group';


@Component({
  selector: 'app-legal-persons',
  templateUrl: './legal-persons.component.html',
  styleUrls: ['./legal-persons.component.css']
})
export class LegalPersonsComponent implements OnInit {

  _legalPersons: Array<LegalPerson> = new Array();

  _setWorkModChosen: Photo[][] = new Array();




  get setWorkModChosen() {
    return this._setWorkModChosen;
  }
  get legalPersons() {
    return this._legalPersons;
  }

  constructor(private bd: BdService) { }

  ngOnInit() {

    /* get models info */
    this.bd.getLegalPersons().subscribe((res) => {

      this._legalPersons = <Array<LegalPerson>> res;
      // console.log('nb of models : ' + this._legalPersons.length);

      /* get images from setWorkMod_ids */
      // if needed, clear 2D array
      // for (let x = 0; x < this._legalPersons.length; x++) {
      //   this._setWorkModChosen[x] = new Array();
      //   for (let y = 0; y < this._legalPersons[x].setWorkMod_ids.length; y++) {
      //     this._setWorkModChosen[x][y] = <Photo>null;
      //   }
      // }

      /* 2D array of photos: i:model, j:model.photo.id */
      for (let i = 0; i < this._legalPersons.length; i++) {
        // console.log('model # : ' + (i + 1) + ' nb of photos : ' + this._legalPersons[i].setWorkMod_ids.length);
        this._setWorkModChosen[i] = new Array();
        for (let j = 0; j < this._legalPersons[i].setWorkMod_ids.length ; j++) {
          this.bd.getPhotoById(this._legalPersons[i].setWorkMod_ids[j]).subscribe((response2) => {
            this._setWorkModChosen[i][j] = <Photo>response2;
            console.log('model # : ' + (i + 1) + ' photo name : ' + this._setWorkModChosen[i][j].title);
          });
        }
      }

    });

  }

}
