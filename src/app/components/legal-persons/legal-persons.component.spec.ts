import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LegalPersonsComponent } from './legal-persons.component';

describe('LegalPersonsComponent', () => {
  let component: LegalPersonsComponent;
  let fixture: ComponentFixture<LegalPersonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LegalPersonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LegalPersonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
