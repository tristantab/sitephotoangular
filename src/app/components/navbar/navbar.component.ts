import { Component, OnInit, OnChanges } from '@angular/core';
import { User } from 'src/app/interfaces/user';
import { Gallery } from 'src/app/interfaces/gallery';
import { BdService } from 'src/app/services/bd.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  // TRY !!!
  // https://www.cssscript.com/path-style-animated-radial-menu-bloomingmenu-js/#tab-pop

   // private sub: any;
  urlState: any;
  yesDisplay = true;
  _galleries: Array<Gallery> = new Array();
  baseUrl = 'http://localhost:4200';

  _users: Array<User>;
  connectedUser: User;
  // needs to get value imported by page where it is => shows searchbar
  // searchEnable: boolean;
  // searchEnable = true;

  get galleries() {
    return this._galleries;
  }
  get users() {
    return this._users;
  }

  // private oneExists: boolean = false;
  // this.oneExists = this.items.filter(user => users.details.id === 1).length > 0;

  constructor(private bd: BdService, private router: Router, private route: ActivatedRoute) { }

  ngOnInit() {

    /* get galleries */
    this.loading();

    /* get user info */
    this.bd.getUsers().subscribe((res) => {
      this._users = <Array<User>> res;
      // test profil choisi
      this.connectedUser = this._users[0];
    });

  };

  /* get galleries from db */
  loading() {
    this.bd.getGalleries().subscribe((response) => {
      this._galleries = <Array<Gallery>> response;
    });
  };

}
