import { Component, OnInit } from '@angular/core';
import { Commentary } from 'src/app/interfaces/commentary';
import { Photo } from 'src/app/interfaces/photo';
import { User } from 'src/app/interfaces/user';
import { BdService } from 'src/app/services/bd.service';

@Component({
  selector: 'app-commentaries',
  templateUrl: './commentaries.component.html',
  styleUrls: ['./commentaries.component.css']
})
export class CommentariesComponent implements OnInit {

  _commentaries: Array<Commentary> = new Array();
  _setWorkComChosen: Photo[] = new Array();
  _setUserComChosen: User[] = new Array();

  get commentaries() {
    return this._commentaries;
  }
  get setWorkComChosen() {
    return this._setWorkComChosen;
  }
  get setUserComChosen() {
    return this._setUserComChosen;
  }

  constructor(private bd: BdService) { }

  ngOnInit() {

    /* get models info */
    this.bd.getCommentaries().subscribe((response) => {

       /* get commentaries */
      this._commentaries = <Array<Commentary>> response;
      // console.log('nb of commentaries : ' + this._commentaries.length);

      /* get users from commentary.id */
      for (let i = 0; i < this._commentaries.length; i++) {
        this.bd.getUserById(this._commentaries[i].user_id).subscribe((response2) => {
          this._setUserComChosen[i] = response2;
          console.log('user name : ' + this._setUserComChosen[i].lastName)
        });
        this.bd.getPhotoById(this._commentaries[i].work_id).subscribe((response3) => {
          this._setWorkComChosen[i] = response3;

        });
      }

    });

  }

}
