import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NavbarComponent } from './components/navbar/navbar.component';
import { BdService } from './services/bd.service';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './components/home/home.component';
import { GalleryComponent } from './components/gallery/gallery.component';
import { UsersComponent } from './components/users/users.component';
import { FooterComponent } from './components/footer/footer.component';
import { CommentariesComponent } from './components/commentaries/commentaries.component';
import { PhotoComponent } from './components/photo/photo.component';
import { LegalPersonsComponent } from './components/legal-persons/legal-persons.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    GalleryComponent,
    UsersComponent,
    FooterComponent,
    CommentariesComponent,
    PhotoComponent,
    LegalPersonsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgbModule
  ],
  providers: [BdService],
  bootstrap: [AppComponent]
})
export class AppModule { }
