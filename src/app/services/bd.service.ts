import { Injectable } from '@angular/core';
import { User } from '../interfaces/user';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Photo } from '../interfaces/photo';
import { Gallery } from '../interfaces/gallery';

@Injectable({
  providedIn: 'root'
})
export class BdService {

  private usersUrl = 'http://localhost:3000/user';

  private photosUrl = 'http://localhost:3000/photo';

  private galleriesUrl = 'http://localhost:3000/gallery';

  getUsers(): Observable<any> {
    return this.http.get('http://localhost:3000/user');
  }

  getUserById(id: number): Observable<User> {
    const url = `${this.usersUrl}/${id}`;
    console.log('service user id passed is : ' + id);
    // console.log('url is : ' + url);
    return this.http.get<User>(url);
  }


  getPhotos(): Observable<any> {
    return this.http.get('http://localhost:3000/photo');
  }

  getPhotoById(id: number): Observable<Photo> {
    const url = `${this.photosUrl}/${id}`;
    console.log('service photo id passed is : ' + id);
    // console.log('url is : ' + url);
    return this.http.get<Photo>(url);
  }


  getGalleries(): Observable<any> {
    return this.http.get('http://localhost:3000/gallery');
  }

  getGalleryById(id: number): Observable<Gallery> {
    const url = `${this.galleriesUrl}/${id}`;
    console.log('service gallery id passed is : ' + id);
    console.log('url is : ' + url);
    return this.http.get<Gallery>(url);
  }


  getCommentaries(): Observable<any> {
    return this.http.get('http://localhost:3000/commentary');
  }

  getLegalPersons(): Observable<any> {
    return this.http.get('http://localhost:3000/legalPerson');
  }

  constructor(private http: HttpClient) { }

}
